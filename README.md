# kubeapps

This guide will walk through the process of setting up Kubeapps and deploy magento app using kubeapps

## Prerequisites

We need to have working kubernetes cluster (ex: RDaaS)
helm 3 client configured to use along with kubectl

## Setup Kubeapps

#### Create namespace for kubeapps

```kubectl create namespace kubeapps```

#### Add bitnami repo

```helm repo add bitnami https://charts.bitnami.com/bitnami```

```helm install kubeapps --namespace kubeapps bitnami/kubeapps --set useHelm3=true ```

#### Expose kubeapps as Loadbalencer Service

```kubectl -n kubeapps edit svc kubeapps```
 
Modify service type from ClusterIP to LoadBalancer and open loadbalencer IP address in browser 

LB IP can be located using following command
```kubectl -n kubeapps get svc kubeapps```

We have created DNS record for above LB IP as `http://kubeapps.kubernauts.sh/`

![](images/kubepps.png)

#### Create API token 

Login to above URL using the token genarated as below:

```kubectl create serviceaccount kubeapps-operator```

```kubectl create clusterrolebinding kubeapps-operator --clusterrole=cluster-admin --serviceaccount=default:kubeapps-operator```

Run following command to get API token for logging into kubeapps dashboard

```kubectl get secret $(kubectl get serviceaccount kubeapps-operator -o jsonpath='{range .secrets[*]}{.name}{"\n"}{end}' | grep kubeapps-operator-token) -o jsonpath='{.data.token}' -o go-template='{{.data.token | base64decode}}' && echo```

Further detailed on kubeapps can be found at ```https://github.com/kubeapps/kubeapps/blob/master/chart/kubeapps/README.md```

## Deploy Magento

#### Create namespace for Magento application

```kubectl create ns magento```

Once after logging into kubeapps dashboard, switch namespace from `default` to `magento` on top right dropdown

![](images/kubeappsdropdown.png)



Select Deploy and search for magento app


![](images/kubeappssearch.png)




Click on Bitnami magento app


![](images/kubeappsmagentoselect.png)




Modify the Name to `magento` and version as `13.1.0`

Replace the yml file content with `./magento-values.yml` from this repo

In this file we are actually maintaining all the prereqisite, custom values for magento helm chart successful deployment or you can update the following values in the editor

Storage class : `storageClass: local-path`
magentoHost: `magentoHost: magento.kubernauts.sh`
magentoUsername: `magentoUsername: kubernauts`
magentoPassword: `magentoPassword: examplepassword`
magentoEmail: `magentoEmail: user@kubernauts.de`
magentoFirstName: `magentoFirstName: Kubernauts`
magentoLastName: `magentoLastName: Administrator`

password and rootUser passwords

and Modify service externalTrafficPolicy from `Cluster` to `Local` to Match LB type

Once we done with above changes, select submit button for deploying magento App

![](images/kubeappsmagentovalues.png)




Once request is submitted, it takes few mins to deploy the pods, service and the magento URLs are accessible as per given `magentoHost` variable in chart values.
The same will be displayed in the chart notes

![](images/magentoAppURLs.png)

